const cl = q => console.log(q)

var drawing = {

	// params 
	container: false,
	imgLeft: false,
	imgRight: false,
	drag: false,
	dragColor: false,

	// init plugins
	init: function(q){

		this.paramsCheck(q)
		this.initSlide()
		this.addImages()

	},
	
	// check params 
	paramsCheck: function(q) {

		q.container ? this.container = q.container : false
		q.imgLeft ? this.imgLeft = q.imgLeft : false
		q.imgRight ? this.imgRight = q.imgRight : false
		q.drag ? this.drag = q.drag : false
		q.dragColor ? this.drag = q.drag : false

	},

	// init slider
	initSlide: function() {

		let maxWidth = $(window).innerWidth()
		$(this.container).append(`
			<div class="drawing-container__item" id=""></div>
			<div class="drawing-container__move" id=""></div>
		`)
		$('.drawing-container__move').slider({
			range: "min",
			value: 45,
			min: 1,
			max: 100,
			slide: function( event, ui ) {

			}
		})
		$('.drawing-container__move').eq(0).find('.ui-slider-range').append(`
		<div class="drawing-container__move--img-left" style="height:100%;width: ${maxWidth}px;position: absolute;top:0;left:0"></div>
	`)

	},

	// Load images and append divs
	addImages: function() {

		$('.drawing-container__item').css({
			'background-image': `url(${this.imgLeft})`
		})
		$('.drawing-container__move--img-left').css({
			'background-image': `url(${this.imgRight})`
		})

	}


	
}