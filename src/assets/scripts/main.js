$(() => {
	$('.soloNumber').each((k, el)=>{
		$(el).on('keydown', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
	})
	$('.input-one').focusin((e) => {
		$(e.target).parent('.input-one-content').addClass('active')
	})
	.focusout((e) => {
		if(!e.target.value.length) {
			$(e.target).parent('.input-one-content').removeClass('active')
		}
	})
	$('.input-one').keyup((e) => {
		if(e.target.value.length) {
			$(e.target).parent('.input-one-content').addClass('active')
		}else{
			$(e.target).parent('.input-one-content').removeClass('active')
		}
	})
})