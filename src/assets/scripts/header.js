$(function(){
	
		//clone div's desktop to menu resposnsive 
		$('.header-list').clone().appendTo('.menu-sidebar-cnt').addClass('menu-responsive').removeClass('header-list');
		$('.header-logo').clone().prependTo('.menu-sidebar-cnt').removeClass('header-logo').addClass('responsive-logo');
		$('.header-languages-select').clone().appendTo('.menu-sidebar-cnt');
	
		//events: menu burguer
		function cerrar_nav() {
			$('.menu-mobile-open').removeClass('active');
			$('.menu-mobile-close').removeClass('active');
			$('.menu-sidebar').removeClass('active');
			$('.header-menu-overlay').removeClass('active');
			$('body').removeClass('hecho');
		};
	
		function abrir_nav(){
			$('.menu-mobile-open').addClass('active');
			$('.menu-mobile-close').addClass('active');
			$('.menu-sidebar').addClass('active');
			$('.header-menu-overlay').addClass('active');
			$('body').addClass('hecho');
			//alert('aladd');
		}
	
		$('.menu-mobile-close , .header-menu-overlay').click(function(event) {
			event.preventDefault();
			cerrar_nav();
		});	
	
		$('.menu-mobile-open').click(function(event) {
			abrir_nav()
		});
	
		//detectando tablet, celular o ipad
		var isMobile = {
			Android: function() {
				return navigator.userAgent.match(/Android/i);
			},
			BlackBerry: function() {
				return navigator.userAgent.match(/BlackBerry/i);
			},
			iOS: function() {
				return navigator.userAgent.match(/iPhone|iPad|iPod/i);
			},
			Opera: function() {
				return navigator.userAgent.match(/Opera Mini/i);
			},
			Windows: function() {
				return navigator.userAgent.match(/IEMobile/i);
			},
			any: function() {
				return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
			}
		};
		
		// dispositivo_movil = $.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))
		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			// tasks to do if it is a Mobile Device
			function readDeviceOrientation() {
				if (Math.abs(window.orientation) === 90) {
					// Landscape
					cerrar_nav();
				} else {
					// Portrait
					cerrar_nav();
				}
			}
			window.onorientationchange = readDeviceOrientation;
		}else{
			$(window).resize(function() {
				var estadomenu = $('.menu-responsive').width();
				if(estadomenu != 0){
					cerrar_nav();
				}
			});
		}
		
	
		// header scroll
		var altoScroll = 0
		$(window).scroll(function() {
			altoScroll = $(window).scrollTop();
			if (altoScroll > 0) {
				$('.header-fixed').addClass('scrolling');
			}else{
				$('.header-fixed').removeClass('scrolling');
			};
		});


		$('.header__enlace__hover__submenu').hover(
			function() {
				$('.header__submenu').addClass('active');
				$('.header__submenu__overlay').addClass('active');
			}, function() {
				$('.header__submenu').removeClass('active');
				$('.header__submenu__overlay').removeClass('active');
			}
		);

		$('.header__nav__session').hover(
			function() {
				$(this).addClass('active');
				$('.header__nav__session__menu').addClass('active');
			}, function() {
				$(this).removeClass('active');
				$('.header__nav__session__menu').removeClass('active');
			}
		);

		$('.header__submenu__acordion').click(function(event) {
			event.preventDefault();
			$('.header__submenu__resp__content').slideToggle('slow');
		});


	});
	